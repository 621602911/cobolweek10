       IDENTIFICATION DIVISION. 
       PROGRAM-ID. OX.
       AUTHOR. PUBODIN.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WS-ROW OCCURS 3 TIMES   .
           05 WS-COL   PIC X OCCURS 3 TIMES VALUE "-".

       01  WS-IDX-ROW PIC 9.
       01  WS-IDX-COL  PIC 9.
       01  WS-INPUT-ROW   PIC 9.
           88 WS-INPUT-ROW-VALID   VALUE 1 THRU 3.
       01  WS-INPUT-COL   PIC  9.
           88 WS-INPUT-COL-VALID   VALUE 1 THRU 3.
       01  WS-COUNT PIC 9(2) VALUE ZERO .
       01  WS-PLAYER PIC X VALUE "X".        
       01  WINNER     PIC X VALUE "-".
           88 WIN      VALUE "X","O".
       
       
           
       PROCEDURE DIVISION .
       BEGIN.
           PERFORM UNTIL WS-COUNT>=9 OR WIN                                     
              PERFORM DISPLAY-TURN
              PERFORM INPUT-ROW-COL
              PERFORM PUT-TABLE               
              PERFORM DISPLAY-TABLE                                             
              PERFORM RESET-ROW-COL
           END-PERFORM 
           PERFORM SHOW-WINNER          
           GOBACK 
       .
       PUT-TABLE.
           IF WS-COL(WS-INPUT-ROW,WS-INPUT-COL) = "-"
              MOVE WS-PLAYER TO WS-COL(WS-INPUT-ROW ,WS-INPUT-COL)
              ADD 1 TO WS-COUNT  
              PERFORM CHECK-WIN                          
              PERFORM TURN-PLAYER
           ELSE 
              DISPLAY "ERROR"
           END-IF 

       .
       DISPLAY-TURN .
           DISPLAY "TURN " WS-PLAYER 
           .
       TURN-PLAYER.
           IF WS-PLAYER = "X"
              MOVE "O" TO WS-PLAYER 
           ELSE
              MOVE "X" TO WS-PLAYER 
           END-IF 
       .
       RESET-ROW-COL.
           MOVE ZERO TO WS-INPUT-ROW,WS-INPUT-COL 
       .
       INPUT-ROW-COL.
           PERFORM UNTIL WS-INPUT-ROW-VALID 
              DISPLAY "INPUT ROW - " WITH NO ADVANCING 
              ACCEPT WS-INPUT-ROW 
           END-PERFORM
           PERFORM UNTIL WS-INPUT-COL-VALID
              DISPLAY "INPUT COL - " WITH NO ADVANCING 
              ACCEPT WS-INPUT-COL 
           END-PERFORM
       .
       DISPLAY-TABLE.
           DISPLAY " "
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3
              PERFORM VARYING WS-IDX-COL FROM 1 BY 1
              UNTIL WS-IDX-COL > 3
                 DISPLAY WS-COL (WS-IDX-ROW, WS-IDX-COL)
                 WITH NO ADVANCING 
               END-PERFORM
               DISPLAY " "
           END-PERFORM
           DISPLAY ""
           
       .
       CHECK-WIN.
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3
              IF WS-COL(WS-IDX-ROW , 1) = WS-COL (WS-IDX-ROW, 2)  AND 
                    WS-COL(WS-IDX-ROW , 2) = WS-COL (WS-IDX-ROW, 3) AND
                       WS-COL(WS-IDX-ROW , 2) IS NOT = "-" THEN  
                          MOVE WS-PLAYER  TO WINNER      
                          
              END-IF
           END-PERFORM
           PERFORM VARYING WS-IDX-COL FROM 1 BY 1
              UNTIL WS-IDX-COL > 3
              IF WS-COL(1 , WS-IDX-COL) = WS-COL (2, WS-IDX-COL)  AND 
                    WS-COL(2 , WS-IDX-COL) = WS-COL (3, WS-IDX-COL) AND
                       WS-COL(2 , WS-IDX-COL) IS NOT = "-" THEN                 
                          MOVE WS-PLAYER TO WINNER    
                          
              END-IF
           END-PERFORM
           IF WS-COL(1 , 1) = WS-COL (2, 2) AND 
                 WS-COL(2 , 2) = WS-COL (3, 3) AND 
                    WS-COL(2 , 2) IS NOT = "-" THEN
                          MOVE WS-PLAYER TO WINNER 
                           
           END-IF
           IF WS-COL(1 , 3) = WS-COL (2, 2) AND 
                 WS-COL(2 , 2) = WS-COL (3, 1) AND 
                    WS-COL(2 , 2) IS NOT = "-" THEN
                          MOVE WS-PLAYER TO WINNER                          
                           
           END-IF
           
       .
       SHOW-WINNER.
           DISPLAY " "
           IF WINNER = "X"
                       DISPLAY "WINNER IS X"
           ELSE 
                IF WINNER = "O"
                          DISPLAY "WINNER IS O"
                END-IF 
                IF WINNER = "-" 
                    DISPLAY "DRAW"
                 END-IF 
            END-IF
       .
